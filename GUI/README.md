# GUI 
The Graphical User Interface for both client and server.

## Troubleshoot

### XAML Editor preview bug 
VS tends to open Avalonia XAML with the wrong Editor.
**Fix:** Right click to the file > Open with ... > Avalonia XAML Editor
