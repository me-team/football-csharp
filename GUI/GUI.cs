﻿using Avalonia;
using Avalonia.Logging.Serilog;
using Avalonia.ReactiveUI;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ME.Football.GUI
{
    public class GUI
    {
        private Thread thread;
        private readonly ThreadStart threadStart;

        public GUI()
        {
            threadStart = new ThreadStart(ThreadEntryPoint);
            thread = new Thread(threadStart);
            thread.Name = "GUI";
        }

        public void Start()
        {
            thread.Start();
        }

        public void WaitForClose()
        {
            thread.Join();
        }

        public Task InvokeAsync(Action action)
        {
            return Avalonia.Threading.Dispatcher.UIThread.InvokeAsync(action);
        }

        public void ThreadEntryPoint()
        {
            Main(null);
        }

        private static void Main(string[] args) => BuildAvaloniaApp()
            .StartWithClassicDesktopLifetime(args);

        // Avalonia configuration, don't remove; also used by visual designer.
        private static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToDebug()
                .UseReactiveUI();
    }
}
