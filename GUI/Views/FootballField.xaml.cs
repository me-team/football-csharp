﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Media;

namespace GUI.Views
{
    public class FootballField : UserControl
    {
        public FootballField()
        {
            this.InitializeComponent();
            //var canvas = this.FindControl<Button>("footballCanvas");
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void Render(DrawingContext context)
        {
            base.Render(context);
            //context.DrawRectangle(new Pen(Colors.Green.ToUint32()), new Rect(0, 0, 300, 300));
            //context.DrawLine(new Pen(Colors.Red.ToUint32(), 4), new Point(0, 0), new Point(100, 100));
        }
    }
}
