﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml.Templates;
using Avalonia.Media;
using ME.Football.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace GUI.Views
{
    public class FootballCanvas : Canvas
    {
        private IList<Player> team1;
        private IList<Player> team2;
        public FootballCanvas()
        {
            team1 = new List<Player>();
            team1.Add(new Player(1, new Vector2(40, 50)));

            team1.Add(new Player(2, new Vector2(170, 70)));

            team2 = new List<Player>();
            team2.Add(new Player(1, new Vector2(150, 300)));
        }

        public override void Render(DrawingContext context)
        {
            base.Render(context);
            foreach (var player in team1)
            {
                context.DrawRectangle(new Pen(Colors.Red.ToUint32(), 5), new Avalonia.Rect(
                    new Point(player.Position.X, player.Position.Y),
                    new Size(5, 5)
                    ));
            }

            foreach (var player in team2)
            {
                context.DrawRectangle(new Pen(Colors.Blue.ToUint32(), 5), new Avalonia.Rect(
                    new Point(player.Position.X, player.Position.Y),
                    new Size(5, 5)
                    ));
            }

        }
    }
}
