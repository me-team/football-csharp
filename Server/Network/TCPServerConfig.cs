﻿using System.Net;

namespace ME.Football.Server.Network
{
    class TCPServerConfig
    {
        public IPEndPoint EndPoint { get; set; }

        /// <summary>
        /// It will be 2
        /// </summary>
        public int RecuiredPlayerForMatch { get; set; }
    }
}
