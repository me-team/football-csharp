﻿using System;
using System.Net.Sockets;
using ME.Football.Model;

namespace ME.Football.Server.Network
{
    public class TcpImplClient : Client
    {
        public TcpClient TcpClient { get; private set; }

        public DateTime LastActivity { get; set; }

        public Boolean WaitingForPingResponse { get; set; }
        
        public TcpImplClient(TcpClient tcpClient)
        {
            TcpClient = tcpClient;
        }
    }
}