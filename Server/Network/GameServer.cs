﻿using ME.Football.Model;
using ME.Football.Server.Model;

namespace ME.Football.Server.Network
{
    /// <summary>
    /// Handles the full communication between the client and server.
    /// </summary>
    class GameServer
    {
        public GameContext Context { get; set; }
    }
}
