﻿using ME.Football.Protocol;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ME.Football.Model;
using ME.Football.Protocol.Message;

namespace ME.Football.Server.Network
{
    class TcpNetworkLayer : INetworkLayer
    {
        public event OnConnectEventHandler OnConnect;
        public event OnDisconnectEventHandler OnDisconnect;
        public event OnMessageReceivedEventHandler OnMessageReceived;

        private readonly TCPServerConfig config;
        private readonly IMessageSerializer serializer;
        private readonly ILogger logger;

        TcpListener listener;
        /// <summary>
        /// Connected but not yet ready for match.
        /// </summary>
        IList<TcpImplClient> connected;
        /// <summary>
        /// The task which waits for the new user
        /// </summary>
        private Task acceptClientTask;
        private CancellationTokenSource acceptClientTaskTokenSrc;

        private Task disconnectCheckTask;
        private CancellationTokenSource disconnectCheckTaskTokenSrc;

        public TcpNetworkLayer(TCPServerConfig config, IMessageSerializer serializer, ILogger logger)
        {
            this.config = config;
            this.serializer = serializer;
            this.logger = logger;
            listener = new TcpListener(config.EndPoint);
            connected = new List<TcpImplClient>();
        }

        public void SendMessage(ProtocolWrapper message, Client client)
        {
            var tcpClient = client as TcpImplClient;
            var msg = serializer.Serialize(message);
            var stream = tcpClient.TcpClient.GetStream();
            lock (client)
            {
                StreamWriter streamWriter = new StreamWriter(stream, Encoding.ASCII);
                var msgString = Encoding.ASCII.GetString(msg);
                streamWriter.WriteLine(msgString);
                streamWriter.Flush();
            }
        }

        public void Start()
        {
            listener.Start();
            logger.Information("Started listening on port: {0}", config.EndPoint.Port);
            
            acceptClientTaskTokenSrc = new CancellationTokenSource();
            acceptClientTask = HandleClientConnectionAsync(acceptClientTaskTokenSrc.Token);
            
            //Comment out if you dont want check
            //StartPingCheck();
        }

        private void StartPingCheck()
        {
            disconnectCheckTaskTokenSrc = new CancellationTokenSource();
            disconnectCheckTask = HandleDisconnectCheck(disconnectCheckTaskTokenSrc.Token);
        }

        private Task HandleDisconnectCheck(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                while (true)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        cancellationToken.ThrowIfCancellationRequested();
                    }
                    
                    
                    foreach (var client in connected)
                    {
                        //Send out pings
                        if (client.WaitingForPingResponse == false)
                        {
                            ProtocolWrapper wrapper = new ProtocolWrapper()
                            {
                                Message = new ME.Football.Protocol.Message.Ping("ping"),
                                MessageType = Messages.PING
                            };
                            SendMessage(wrapper, client);
                        }
                        else //Remove
                        {
                            connected.Remove(client);
                            OnDisconnect?.Invoke(client as Client);
                        }
                    }
                    
                    Task.Delay(2000);
                }
            }, cancellationToken);
        }

        public void Stop()
        {
            acceptClientTaskTokenSrc.Cancel();
            listener.Stop();
        }

        /// <summary>
        /// Handles client connection
        /// </summary>
        private async Task HandleClientConnectionAsync(CancellationToken cancellationToken = default)
        {
            var newClient = await listener.AcceptTcpClientAsync();

            if (cancellationToken.IsCancellationRequested)
            {
                cancellationToken.ThrowIfCancellationRequested();
                return;
            }
            
            logger.Information("[NetworkLayer] New client connected with IP: {0}", newClient.Client.RemoteEndPoint.ToString());
            var tcpClient = new TcpImplClient(newClient)
            {
                GUID = Guid.NewGuid().ToString(),
                LastActivity = DateTime.Now
            };
            connected.Add(tcpClient);
            OnConnect?.Invoke();
            
            HandleClientMessageAsync(tcpClient);

            //Waiting for the new connection
            //TODO: Can recursive cause stuck data in memory?
            HandleClientConnectionAsync(cancellationToken);
        }

        private async void HandleClientMessageAsync(TcpImplClient client)
        {
            var stream = client.TcpClient.GetStream();
            using(StreamReader streamReader = new StreamReader(stream, Encoding.ASCII))
            {
                while (true)
                {
                    if (stream.DataAvailable)
                    {
                        string line = await streamReader.ReadLineAsync();
                        logger.Information("[NetworkLayer] Message received: {0}", line);

                        var msg = serializer.Deserializer(line);
                        OnMessageReceived?.Invoke(msg, client);
                    }
                    await Task.Delay(10);
                }
            }
        }

    }
}
