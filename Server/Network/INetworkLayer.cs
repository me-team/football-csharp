﻿using ME.Football.Protocol;
using System;
using System.Collections.Generic;
using System.Text;
using ME.Football.Model;

namespace ME.Football.Server.Network
{
    internal delegate void OnConnectEventHandler();
    internal delegate void OnDisconnectEventHandler(Client client);
    internal delegate void OnMessageReceivedEventHandler(object message, Client client);
    interface INetworkLayer
    {
        void Start();
        void Stop();
        void SendMessage(ProtocolWrapper message, Client client);

        event OnConnectEventHandler OnConnect;
        event OnDisconnectEventHandler OnDisconnect;
        event OnMessageReceivedEventHandler OnMessageReceived;
    }
}
