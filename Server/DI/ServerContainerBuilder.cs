﻿using Autofac;
using ME.Football.DI;
using ME.Football.Protocol;
using ME.Football.Server.Network;
using ME.Football.Server.Protocol;
using System.Net;

namespace ME.Football.Server.DI
{
    class ServerContainerBuilder : CoreContainerBuilder
    {
        public ServerContainerBuilder() : base()
        {
            
        }

        public ServerContainerBuilder UseJSONSerialization()
        {
            this.RegisterType<JsonMessageSerializer>().As<IMessageSerializer>();
            return this;
        }
        public ServerContainerBuilder UseTCPNetworkLayer()
        {
            var config = new TCPServerConfig()
            {
                EndPoint = new IPEndPoint(IPAddress.Any, 9999),
                RecuiredPlayerForMatch = 2
            };
            this.RegisterInstance(config).As<TCPServerConfig>();
            this.RegisterType<TcpNetworkLayer>().As<INetworkLayer>();
            return this;
        }
        public ServerContainerBuilder UseDefaultServerLogic()
        {
            this.RegisterType<ServerLogicImpl>().As<IServerLogic>();
            this.RegisterType<ServerProtocolLogicImpl>().As<ServerProtocolLogic>();
            return this;
        }
    }
}
