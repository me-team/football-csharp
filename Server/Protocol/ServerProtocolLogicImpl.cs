﻿using ME.Football.Model;
using ME.Football.Protocol;
using ME.Football.Protocol.Message;
using System;
using System.Collections.Generic;
using ME.Football.Protocol.Message.Server;

namespace ME.Football.Server.Protocol
{
    /// <summary>
    /// The standard protocol determined by: https://gitlab.com/me-team/docs
    /// </summary>
    class ServerProtocolLogicImpl : ServerProtocolLogic
    {
        public ServerProtocolLogicImpl(IMessageSerializer parser) : base(parser)
        {
        }

        public override byte[] First()
        {
            var formations = new List<Formation>()
            {
                new Formation()
                {
                    Id = 1,
                    Spots = new List<Player>()
                    {
                        new Player(1, new Vector2(1, 2))
                    }
                }
            };
            Formations msg = new Formations()
            {
                AvaibleFormations = formations
            };

            return parser.Serialize(msg);
        }

        public override byte[] GetResponse(byte[] clientMessage)
        {
            throw new NotImplementedException();
        }
    }
}
