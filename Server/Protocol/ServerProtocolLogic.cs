﻿using ME.Football.Protocol;

namespace ME.Football.Server.Protocol
{
    /// <summary>
    /// Generate response to the client depends on the response
    /// </summary>
    abstract class ServerProtocolLogic
    {
        internal IMessageSerializer parser;

        public ServerProtocolLogic(IMessageSerializer parser)
        {
            this.parser = parser;
        }

        public abstract byte[] First();

        public abstract byte[] GetResponse(byte[] clientMessage);
    }
}
