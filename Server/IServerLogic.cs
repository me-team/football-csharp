﻿namespace ME.Football.Server
{
    /// <summary>
    /// Main loop of the server. Handles client connection and initial communication.
    /// </summary>
    interface IServerLogic
    {
        void Start();
        void Stop();
        void Loop();

    }
}
