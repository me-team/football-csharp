﻿using ME.Football.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ME.Football.Server.UI
{
    /// <summary>
    /// Extended common game events with server specific events for the UI
    /// </summary>
    interface ServerUserInterface : UserInterface
    {
    }
}
