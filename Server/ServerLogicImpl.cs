﻿using ME.Football.Server.Network;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using ME.Football.Model;
using ME.Football.Protocol;
using ME.Football.Protocol.Message;
using ME.Football.Protocol.Message.Client;
using ME.Football.Protocol.Message.Server;
using ME.Football.Server.Model;
using Serilog;

namespace ME.Football.Server
{
    class ServerLogicImpl : IServerLogic
    {
        private readonly INetworkLayer networkLayer;
        private readonly ILogger logger;

        private readonly object lobbyMutex = new object();
        private IList<Client> lobby;

        public ServerLogicImpl(INetworkLayer networkLayer, ILogger logger)
        {
            this.networkLayer = networkLayer;
            this.logger = logger;
            lobby = new List<Client>();
            
            networkLayer.OnConnect += NetworkLayer_OnConnect;
            networkLayer.OnDisconnect += NetworkLayer_OnDisconnect;
            networkLayer.OnMessageReceived += NetworkLayer_OnMessageReceived;
        }

        private void NetworkLayer_OnMessageReceived(object msg, Client client)
        {
            if (msg.GetType() == typeof(Bonjour))
            {
                var message = msg as Bonjour;
                client.Name = message.UserName;
                SendFormations(client);
            }

            if (msg.GetType() == typeof(SelectedFormation))
            {
                // Add to lobby
                lock (lobby)
                {
                    lobby.Add(client);
                }
                logger.Information("{0} client added to the lobby.", client.Name);
                
                if (lobby.Count >= 2)
                {
                    StartNewGameServerFromLobby();
                }
                else
                {
                    SendWaintingForPlayer(client);
                }
            }
        }

        private void StartNewGameServerFromLobby()
        {           
            Client client1 = lobby[0];
            Client client2 = lobby[1];
            GameContext context = new GameContext()
            {
                Client1 = client1,
                Client2 = client2
            };
            StartNewGameServer(context);
            lobby.Remove(client1);
            lobby.Remove(client2);
        }

        private void StartNewGameServer(GameContext context)
        {
            logger.Information("Starting new game server...");
            logger.Information("Picked players from lobby: \n - {0}\n - {1}", context.Client1.Name, context.Client2.Name);
            SendGameStart(context.Client1, context.Client2);
            SendGameStart(context.Client2, context.Client1);
            
            GameServer gameServer = new GameServer()
            {
                Context = context
            };
        }

        private void SendGameStart(Client client, Client opponent)
        {
            var wrapper = new ProtocolWrapper()
            {
                Message = new StartGame()
                {
                    Opponent = null
                },
                MessageType = Messages.START_GAME
            };
            networkLayer.SendMessage(wrapper, client);
        }

        private void SendWaintingForPlayer(Client client)
        {
            //throw new NotImplementedException();
        }

        private void SendFormations(Client client)
        {
            var formations = new List<Formation>();
            formations.Add(new Formation()
            {
                Id = 1,
                Spots = new List<Player>()
                {
                }
            });
            var packet = new ProtocolWrapper()
            {
                Message = new Formations()
                {
                    AvaibleFormations = formations
                },
                MessageType = Messages.FORMATIONS
            };
            networkLayer.SendMessage(packet,
                client);
        }

        private void NetworkLayer_OnDisconnect(Client client)
        {
            lock (lobbyMutex)
            {
                lobby.Remove(client);
            }
        }

        private void NetworkLayer_OnConnect()
        {
            // lock (lobbyMutex)
            // {
            //     
            // }
            //throw new NotImplementedException();
        }

        public void Loop()
        {
            while (true)
            {
                Thread.Sleep(100);
            }
        }

        public void Start()
        {
            networkLayer.Start();
        }

        public void Stop()
        {
            networkLayer.Stop();
        }
    }

}
