﻿using ME.Football.Model;

namespace ME.Football.Server.Model
{
    public class GameContext
    {
        public Client Client1 { get; set; }
        public Client Client2 { get; set; }

        public Formation Client1Formation { get; set; }
        public Formation Client2Formation { get; set; }
    }
}