﻿using Autofac;
using ME.Football.Server.DI;
using System.Collections.Generic;
using ME.Football.UI;

namespace ME.Football.Server
{
    /// <summary>
    /// Entry point of the lib
    /// </summary>
    /// 
    public class Server
    {
        IServerLogic serverLogic;
        private readonly IContainer container;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInterfaces">Pass the user interfaces which should be handled by the server</param>
        public Server(IList<UserInterface> userInterfaces = null)
        {
            container = InitContainer();   
        }

        private IContainer InitContainer()
        {
            ServerContainerBuilder builder = new ServerContainerBuilder();
            builder.UseSerilog();
            builder
                .UseJSONSerialization()
                .UseTCPNetworkLayer()
                .UseDefaultServerLogic();
            return builder.Build();
        }

        public void Start()
        {
            using (var scope = container.BeginLifetimeScope())
            {
                serverLogic = scope.Resolve<IServerLogic>();
                serverLogic.Start();
                serverLogic.Loop();
            }
        }
    }
}
