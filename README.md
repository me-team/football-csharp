# Football CSharp
This application is made for an University assignment. (Hungary, University of Miskolc)

The game is a 2D football game which clients are bots.
The goal is to create a server which can communicate on a given protocol with any client. The game is kind of a "turn based" football. The server should be able to handle an imaginary programming competition where every competitors goal is to create a football AI.


# Building the project
Steps to build the app.

## Requirements 
- [Install .NET Core to your system](https://dotnet.microsoft.com/download)

## Build & run
```bash
cd Server.CLI #or Client.CLI
dotnet run
```
