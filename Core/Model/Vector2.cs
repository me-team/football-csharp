﻿namespace ME.Football.Model
{
    public class Vector2
    {

        public Vector2(int v1, int v2)
        {
            this.X = v1;
            this.Y = v2;
        }

        public double X { get; set; }
        public double Y { get; set; }
    }
}