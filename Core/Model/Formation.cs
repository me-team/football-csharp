﻿using ME.Football.Model.Enums;
using System.Collections.Generic;

namespace ME.Football.Model
{
    public class Formation
    {
        public int Id { get; set; }
        public Vector2 Position { get; set; }
        public ICollection<Player> Spots { get; set; }
        public int fieldWidth { get; private set; }
        public int fieldHeight { get; private set; }



        private void FormationFourFourTwo(Vector2 Position)
        {
            int[,] fourFourTwoHomeKickOff = new int[,] { { 3, 33 }, { 20, 10 }, { 20, 20 }, { 20, 40 }, { 20, 50 }, { 30, 10 }, { 30, 20 }, { 30, 40 }, { 30, 50 }, { 40, 40 }, { 40, 50 } };


            /*Vector2[] fourFourTwoHomeNotKickoff = new Vector2[11];
            fourFourTwoHomeNotKickoff[0] = new Vector2(3, 33);
            fourFourTwoHomeNotKickoff[1] = new Vector2(20, 10);
            fourFourTwoHomeNotKickoff[2] = new Vector2(20, 20);
            fourFourTwoHomeNotKickoff[3] = new Vector2(20, 40);
            fourFourTwoHomeNotKickoff[4] = new Vector2(20, 50);
            fourFourTwoHomeNotKickoff[5] = new Vector2(30, 10);
            fourFourTwoHomeNotKickoff[6] = new Vector2(30, 20);
            fourFourTwoHomeNotKickoff[7] = new Vector2(30, 40);
            fourFourTwoHomeNotKickoff[8] = new Vector2(30, 50);
            fourFourTwoHomeNotKickoff[9] = new Vector2(40, 20);
            fourFourTwoHomeNotKickoff[10] = new Vector2(40, 40);

            Vector2[] fourFourTwoHomeKickoff = new Vector2[11];
            fourFourTwoHomeKickoff[0] = new Vector2(3, 33);
            fourFourTwoHomeKickoff[1] = new Vector2(20, 10);
            fourFourTwoHomeKickoff[2] = new Vector2(20, 20);
            fourFourTwoHomeKickoff[3] = new Vector2(20, 40);
            fourFourTwoHomeKickoff[4] = new Vector2(20, 50);
            fourFourTwoHomeKickoff[5] = new Vector2(30, 10);
            fourFourTwoHomeKickoff[6] = new Vector2(30, 20);
            fourFourTwoHomeKickoff[7] = new Vector2(30, 40);
            fourFourTwoHomeKickoff[8] = new Vector2(30, 50);
            fourFourTwoHomeKickoff[9] = new Vector2(50, 33);
            fourFourTwoHomeKickoff[10] = new Vector2(50, 30);

            Vector2[] fourFourTwoAwayNotKickoff = new Vector2[11];
            fourFourTwoAwayNotKickoff[0] = new Vector2(97, 33);
            fourFourTwoAwayNotKickoff[1] = new Vector2(80, 10);
            fourFourTwoAwayNotKickoff[2] = new Vector2(80, 20);
            fourFourTwoAwayNotKickoff[3] = new Vector2(80, 40);
            fourFourTwoAwayNotKickoff[4] = new Vector2(80, 50);
            fourFourTwoAwayNotKickoff[5] = new Vector2(70, 10);
            fourFourTwoAwayNotKickoff[6] = new Vector2(70, 20);
            fourFourTwoAwayNotKickoff[7] = new Vector2(70, 40);
            fourFourTwoAwayNotKickoff[8] = new Vector2(70, 50);
            fourFourTwoAwayNotKickoff[9] = new Vector2(60, 33);
            fourFourTwoAwayNotKickoff[10] = new Vector2(60, 33);

            Vector2[] fourFourTwoAwayKickoff = new Vector2[11];
            fourFourTwoAwayKickoff[0] = new Vector2(97, 33);
            fourFourTwoAwayKickoff[1] = new Vector2(80, 10);
            fourFourTwoAwayKickoff[2] = new Vector2(80, 20);
            fourFourTwoAwayKickoff[3] = new Vector2(80, 40);
            fourFourTwoAwayKickoff[4] = new Vector2(80, 50);
            fourFourTwoAwayKickoff[5] = new Vector2(70, 10);
            fourFourTwoAwayKickoff[6] = new Vector2(70, 20);
            fourFourTwoAwayKickoff[7] = new Vector2(70, 40);
            fourFourTwoAwayKickoff[8] = new Vector2(70, 50);
            fourFourTwoAwayKickoff[9] = new Vector2(50, 33);
            fourFourTwoAwayKickoff[10] = new Vector2(50, 33);*/

        }
    }
}
