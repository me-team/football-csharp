﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ME.Football.Model
{
    class TeamGenerator
    {
        public int Id { get; set; }
        public Vector2 Position { get; set; }
        public ICollection<Player> Spots { get; set; }
        public Player[] Players { get; set; }
        public Formation Formation { get; }

        public void initTeams(int Id, Vector2[] Position)
        {

            //Init HomeTeam(team1)
            for(int i = 0; i<11; i++)
            {
                //Init based on the actual gamestate (home/away, kickoff/notkickoff)
                Players[i] = new Player(i, Position[i]);
            }

            //Init AwayTeam(team2)
            for (int i = 0; i < 11; i++)
            {
                //Init based on the actual gamestate (home/away, kickoff/notkickoff)
                Players[i] = new Player(i, Position[i]);
            }
        }

    }
}
