﻿using ME.Football.Protocol;
using System;
using System.Collections.Generic;
using System.Text;

namespace ME.Football.Model
{
    public abstract class Client
    {
        public string Name { get; set; }
        public string GUID { get; set; }
    }
}
