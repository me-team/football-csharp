using System.Collections.Generic;

namespace ME.Football.Model
{
    /// <summary>
    /// Full state of a game match
    /// </summary>
    /// 

    public class GameState
    {
        public Team Team1 { get; set; }
        public Team Team2 { get; set; }
    }
}