﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ME.Football.Model.Enums
{
    /// <summary>
    /// List of goal types.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum GoalType
    {
        REGULAR,
        OWN_GOAL
    }
}
