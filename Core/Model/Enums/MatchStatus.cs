﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace ME.Football.Model.Enums
{
    /// <summary>
    /// List of statuses.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MatchStatus
    {
        KICKOFF,
        PLAYING,
        HALFTIME,
        PAUSED,
        FINISHED
    }
}
