﻿using ME.Football.Model.Enums;

namespace ME.Football.Model
{
    public class Ball
    {
        public Vector2 Position { get; set; }
        public Vector2 Direction { get; set; }
        public float Speed { get; set; }

    }
}