﻿using ME.Football.Model.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ME.Football.Model
{
    public class Goal
    {
        public short Time { get; set; }
        public Team Team { get; set; }
        public Player Scorer { get; set; }

        //public GoalType Type;

        /// <summary>
        /// String format.
        /// </summary>
        /// <returns>Object in string format.</returns>
        public override string ToString()
        {
            return $"{Scorer.Id} ({Time})";
        }

    }
}
