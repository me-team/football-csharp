﻿using System;

namespace ME.Football.Model
{
    public class Team
    {
        public int Id { get; set; }
        public Player[] Players { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Direction { get; set; }
    }
}