﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ME.Football.Model
{
    /// <summary>
    /// Football player
    /// </summary>
    public class Player
    {

        public int Id { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Direction { get; set; }
        public ICollection<Player> Spots { get; set; }

        public Player(int Id, Vector2 Position)
        {
            this.Id = Id;
            this.Position = Position;
        }

    }
}