﻿using System;
using System.Collections.Generic;
using System.Text;
using ME.Football.Model;
using ME.Football.Model.Enums;

namespace ME.Football.Model
{
    class GameLogic
    {
        // After start, starting team's p[9] kicks the ball down, to p[10]
        // The main aim is to only move the players in the ball's given radius, so no bunkerball goes on.
        // If the ball gets out of the actually moving players' radius, players go back to original position.
        // At the first version, probably no passing will exist, only kick, but the players want to possess the ball asap.
        // A player who possessing the ball is facing towards the opponent's goal.
        // When a player is containing the ball, the player and the ball has the same (X,Y) vectors (or maybe one pixel to the opponent's goal), but if an opponent wants to get the ball, he has to tackle him.
        // If the attacking player (from the team which isn't possessing) gets to the possessing player, he tackles him and the ex-possesser now gets tackled a few coordinates away.
        // With this logic we might eliminate an endless possession-switch.
        // Goalkeeper stays in place, maximum reach is the penalty box (l.s.:{(25,0),(25,16),(41,16),(41,0), r.s:(25,100),(25,84),(41,84),(41,100)).
        // Goalkeeper only moves from the starting (3,33) when the opponent striker gets in the radius, eg: l.s:11+r20, r.s:89-r20

        public int Id { get; set; }
        public Player[] Players { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Direction { get; set; }
        public Formation Formation { get; set; }
        public int fieldWidth { get; private set; }
        public int fieldHeight { get; private set; }

        public MatchStatus actualMatchStatus;
        private bool moving;
        
        public void MatchLogic(Ball Ball, Player Player, Formation Formation, Vector2 Position, Vector2 Direction, float Speed)
        {
            double speed = 100;
            double elapsed = 0.01f;
            double distanceToBallTeam1;
            double distanceToBallTeam2;
            double distanceToGoalTeam1;
            double distanceToGoalTeam2;
            double fieldWidth = 100;
            double fieldHeight = 64;
            double homePenaltyBoxX = 16;
            double awayPenaltyBoxX = 84;
            int goalTopPostY = 36;
            int goalBottomPostY = 30;
            int distanceToBallChasingRadius = 20;
            Random rnd = new Random();
            double aimOnGoalY;

            int ballAttached = 9;
            int possessingTeam = 1;
            Team[] team1 = new Team[11];
            Team[] team2 = new Team[11];

            float[,] fourFourTwoHomeKickOff = new float[,] { { 3, 32 }, { 20, 10 }, { 20, 20 }, { 20, 40 }, { 20, 50 }, { 30, 10 }, { 30, 20 }, { 30, 40 }, { 30, 50 }, { 50, 33 }, { 50, 30 } };
            float[,] fourFourTwoHomeNotKickOff = new float[,] { { 3, 32 }, { 20, 10 }, { 20, 20 }, { 20, 40 }, { 20, 50 }, { 30, 10 }, { 30, 20 }, { 30, 40 }, { 30, 50 }, { 40, 20 }, { 40, 40 } };
            float[,] fourFourTwoAwayKickOff = new float[,] { { 97, 32 }, { 80, 10 }, { 80, 20 }, { 80, 40 }, { 80, 50 }, { 70, 10 }, { 70, 20 }, { 70, 40 }, { 70, 50 }, { 50, 33 }, { 50, 30 } };
            float[,] fourFourTwoAwayNotKickOff = new float[,] { { 97, 32 }, { 80, 10 }, { 80, 20 }, { 80, 40 }, { 80, 50 }, { 70, 10 }, { 70, 20 }, { 70, 40 }, { 70, 50 }, { 60, 20 }, { 60, 40 } };


            if (actualMatchStatus == MatchStatus.KICKOFF)
            {
                //Ball init
                Ball.Position.X = fieldWidth / 2; //50
                Ball.Position.Y = fieldHeight / 2; //32

                //Players init for first kickoff or after the second team scored a goal
                for (int i = 0; i < 11; i++)
                {
                    //Probably bad syntax
                    team1[i].Position.X = fourFourTwoHomeKickOff[i, 0];
                    team1[i].Position.Y = fourFourTwoHomeKickOff[0, i];
                    team2[i].Position.X = fourFourTwoAwayNotKickOff[i, 0];
                    team2[i].Position.Y = fourFourTwoAwayNotKickOff[0, i];
                }

                //kickoff happens, then gamestatus changes to playing
                Ball.Position.X = fieldWidth / 2;
                Ball.Position.Y = 30;
                ballAttached = 10;
                possessingTeam = 1;
                //PLAYING

            }
            else if (actualMatchStatus == MatchStatus.PLAYING && (Ball.Position.X == 100 && (Ball.Position.Y >= 30 && Ball.Position.Y <= 36)))
            {
                //Team 1 scores a goal
                actualMatchStatus = MatchStatus.KICKOFF;
            }
            else if (actualMatchStatus == MatchStatus.PLAYING && (Ball.Position.X == 0 && (Ball.Position.Y >= 30 && Ball.Position.Y <= 36)))
            {
                //Team 2 scores a goal
                actualMatchStatus = MatchStatus.KICKOFF;
            }
            //Ball bounces back from the edges
            else if (actualMatchStatus == MatchStatus.PLAYING && (Position.X < 0 || Position.X > fieldWidth)) //-ballSize/2
            {
                Ball.Direction.X *= -1;
            }
            else if (actualMatchStatus == MatchStatus.PLAYING && (Position.Y < 0 || Position.Y > fieldHeight)) //-ballSize/2
            {
                Ball.Direction.Y *= -1;
            }
            else
            {
                while (actualMatchStatus == MatchStatus.PLAYING)
                {
                    for (int i = 1; i < 11; i++)
                    {
                        if (team1[i].Position.X == Ball.Position.X && team1[i].Position.Y == Ball.Position.Y) //First team's i-th player contains the ball
                        {
                            ballAttached = i;
                            possessingTeam = 1;
                            distanceToGoalTeam1 = Math.Sqrt(Math.Pow(100 - team1[i].Position.X, 2) + Math.Pow((fieldHeight/2) - team1[i].Position.Y, 2));
                            if (distanceToGoalTeam1 > homePenaltyBoxX) // Carry to the penalty area - I know it's a magic number, but the areas of the field are constant
                            {
                                // move towards the goal, and carry the ball
                                team1[i].Position.X = team1[i].Position.X + speed * awayPenaltyBoxX / distanceToGoalTeam1; //Don't carry to the goal line, has to shoot from the penalty area eg
                                team1[i].Position.Y = team1[i].Position.Y + speed * (fieldHeight/2) / distanceToGoalTeam1;
                                Ball.Position.X = team1[i].Position.X;
                                Ball.Position.Y = team1[i].Position.Y;
                            }
                            else if (distanceToGoalTeam1 <= homePenaltyBoxX) //Shot on target
                            {
                                aimOnGoalY = (double)rnd.Next((goalBottomPostY * 10), (goalTopPostY * 10) + 1) /10; //Randomly chooses a point on the goal line
                                Ball.Position.X = Ball.Position.X + speed * 100 / distanceToGoalTeam1;
                                Ball.Position.Y = Ball.Position.Y + speed * aimOnGoalY / distanceToGoalTeam1;
                            }

                        }
                        else if (team2[i].Position.X == Ball.Position.X && team2[i].Position.Y == Ball.Position.Y) //Second team's i-th player contains the ball
                        {
                            ballAttached = i;
                            possessingTeam = 2;
                            distanceToGoalTeam2 = Math.Sqrt(Math.Pow(0 - team1[i].Position.X, 2) + Math.Pow((fieldHeight/2) - team1[i].Position.Y, 2));
                            if (distanceToGoalTeam2 > homePenaltyBoxX)
                            {
                                // move towards the goal, and carry the ball
                                team2[i].Position.X = team2[i].Position.X + speed * homePenaltyBoxX / distanceToGoalTeam2; //Don't carry to the goal line, has to shoot from the penalty area eg
                                team2[i].Position.Y = team2[i].Position.Y + speed * (fieldHeight/2) / distanceToGoalTeam2;
                                Ball.Position.X = team2[i].Position.X;
                                Ball.Position.Y = team2[i].Position.Y;
                            }
                            else if (distanceToGoalTeam2 <= homePenaltyBoxX) //Shot on target
                            {
                                aimOnGoalY = (double)rnd.Next((goalBottomPostY * 10), (goalTopPostY * 10) + 1) / 10; //Randomly chooses a point on the goal line
                                Ball.Position.X = Ball.Position.X + speed * 100 / distanceToGoalTeam2;
                                Ball.Position.Y = Ball.Position.Y + speed * aimOnGoalY / distanceToGoalTeam2;
                            }
                        }
                        else //No one contains the ball, trying to get it, but only the players will move in given distance (no bunkerball)
                        {
                            distanceToBallTeam1 = Math.Sqrt(Math.Pow(Ball.Position.X - team1[i].Position.X, 2) + Math.Pow(Ball.Position.Y - team1[i].Position.Y, 2));
                            distanceToBallTeam2 = Math.Sqrt(Math.Pow(Ball.Position.X - team2[i].Position.X, 2) + Math.Pow(Ball.Position.Y - team2[i].Position.Y, 2));

                            if (distanceToBallTeam1 <= distanceToBallChasingRadius)
                            {
                                //starting running towards the ball, getting the direction
                                team1[i].Direction.X = (Ball.Position.X - team1[i].Position.X) / distanceToBallTeam1;
                                team1[i].Direction.Y = (Ball.Position.Y - team1[i].Position.Y) / distanceToBallTeam1;
                                moving = true;

                                if (moving == true)
                                {
                                    team1[i].Position.X += team1[i].Direction.X * speed * elapsed;
                                    team1[i].Position.Y += team1[i].Direction.Y * speed * elapsed;
                                    if (Math.Sqrt(Math.Pow(Ball.Position.X - team1[i].Position.X, 2) + Math.Pow(Ball.Position.Y - team1[i].Position.Y, 2)) >= distanceToBallTeam1)
                                    {
                                        team1[i].Position.X = Ball.Position.X;
                                        team1[i].Position.Y = Ball.Position.Y;
                                        moving = false;
                                    }
                                }
                            }

                            if (distanceToBallTeam2 <= distanceToBallChasingRadius)
                            {
                                //starting running towards the ball, getting the direction
                                team2[i].Direction.X = (Ball.Position.X - team2[i].Position.X) / distanceToBallTeam2;
                                team2[i].Direction.Y = (Ball.Position.Y - team2[i].Position.Y) / distanceToBallTeam2;
                                moving = true;

                                if (moving == true)
                                {
                                    team2[i].Position.X += team2[i].Direction.X * speed * elapsed;
                                    team2[i].Position.Y += team2[i].Direction.Y * speed * elapsed;
                                    if (Math.Sqrt(Math.Pow(Ball.Position.X - team2[i].Position.X, 2) + Math.Pow(Ball.Position.Y - team2[i].Position.Y, 2)) >= distanceToBallTeam2)
                                    {
                                        team1[i].Position.X = Ball.Position.X;
                                        team1[i].Position.Y = Ball.Position.Y;
                                        moving = false;
                                    }
                                }
                            }


                        }
                    }
                    
                }
            }
        }


    }
}
