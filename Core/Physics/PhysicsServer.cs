﻿using System;
using System.Timers;
using ME.Football.Model;
using ME.Football.Protocol.Message;

namespace ME.Football.Physics
{
    public class PhysicsServer
    {
        // By arrangement we use 100x64 field size, to make it dividable by 2.
        // The goal line is 6 metres long, so the posts are at Y=30 and Y=36, if the ball is there, a team scores a goal.

        public int fieldWidth = 100;
        public int fieldHeight = 64;

        /// <summary>
        /// Create this at the beginning of the game with the selected formations
        /// </summary>
        /// <param name="team1">Selected by client1</param>
        /// <param name="team2">Selected by client2</param>
        public PhysicsServer(Formation team1, Formation team2)
        {

        }

        /// <summary>
        /// Calculate the new state with the given client updates
        /// team1 or team2 can be null or both
        /// </summary>
        /// <param name="team1"></param>
        /// <param name="team2"></param>
        /// <returns>new state</returns>
        public GameState calculateNewState(GameUpdate team1, GameUpdate team2)
        {
            throw new NotImplementedException();
        }

        public static void Timer()
        {
            Timer myTimer = new Timer();
            myTimer.Elapsed += new ElapsedEventHandler(DisplayTimeEvent);
            myTimer.Interval = 100;
            myTimer.Start();

            while (Console.Read() != 'q')
            {
                ;    // do nothing...
            }
        }
        public static void DisplayTimeEvent(object source, ElapsedEventArgs e)
        {
            Console.WriteLine("\r{0}", DateTime.Now);
        }

    }




}
