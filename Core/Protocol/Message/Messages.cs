﻿namespace ME.Football.Protocol.Message
{
    public enum Messages
    {
        PING = 0,
        BONJORE = 1,
        FORMATIONS = 2,
        SELECTED_FORMATION = 3,
        START_GAME = 4,
    }
}
