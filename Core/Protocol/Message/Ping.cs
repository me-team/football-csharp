﻿namespace ME.Football.Protocol.Message
{
    public class Ping
    {
        public string PingMsg { get; }
        public Ping(string pingMsg)
        {
            PingMsg = pingMsg;
        }

    }
}