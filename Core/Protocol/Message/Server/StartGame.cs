﻿using System.Collections.Generic;

namespace ME.Football.Protocol.Message.Server
{
    public class StartGame
    {
        public Model.Client Opponent { get; set; }
    }
}