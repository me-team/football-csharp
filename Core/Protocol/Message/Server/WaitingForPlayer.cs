﻿namespace ME.Football.Protocol.Message.Server
{
    public class WaitingForPlayer
    {
        public int AvailablePlayers { get; set; }
        public int NeededPlayerCount { get; set; }
    }
}