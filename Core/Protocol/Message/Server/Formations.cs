﻿using System;
using System.Collections.Generic;
using System.Text;
using ME.Football.Model;

namespace ME.Football.Protocol.Message.Server
{
    public class Formations
    {
        public IList<Formation> AvaibleFormations { get; set; }
    }
}
