﻿using ME.Football.Model;

namespace ME.Football.Protocol.Message.Client
{
    public class SelectedFormation
    {
        public int FormationId { get; set; }
    }
}