﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ME.Football.Protocol.Message.Client
{
    /// <summary>
    /// First msg from client
    /// </summary>
    public class Bonjour
    {
        public string VersionName { get; set; }

        public string UserName { get; set; }
    }
}
