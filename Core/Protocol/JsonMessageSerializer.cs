﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Text;
using ME.Football.Protocol.Message;
using ME.Football.Protocol.Message.Client;
using ME.Football.Protocol.Message.Server;
using Newtonsoft.Json.Linq;
using Serilog;

namespace ME.Football.Protocol
{
    /// <summary>
    /// Parsing JSON
    /// </summary>
    public class JsonMessageSerializer : IMessageSerializer
    {
        private readonly ILogger logger;
        private readonly JsonSerializerSettings jsonSerializerSettings;

        public JsonMessageSerializer(ILogger logger)
        {
            this.logger = logger;
            var contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            };
            jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                Formatting = Formatting.None
//#if DEBUG
//                Formatting = Formatting.Indented // Do not use line break means new message in TCP comm
//#endif
            };
        }

        public ProtocolWrapper DeserializerWrapper(string message)
        {
            return JsonConvert.DeserializeObject<ProtocolWrapper>(message);
        }

        public object Deserializer(string message)
        {
            var wrapper = DeserializerWrapper(message);
            switch (wrapper.MessageType)
            {
                case Messages.BONJORE:
                    return (wrapper.Message as JObject).ToObject<Bonjour>();
                case Messages.FORMATIONS:
                    return (wrapper.Message as JObject).ToObject<Formations>();
                case Messages.SELECTED_FORMATION:
                    return (wrapper.Message as JObject).ToObject<SelectedFormation>();
            }
            logger.Error("[Serialization] Unknown message type. Can't parse.");
            return null;
        }

        public byte[] Serialize(object message)
        {
            string srt = JsonConvert.SerializeObject(message, jsonSerializerSettings);
            return Encoding.ASCII.GetBytes(srt);
        }
    }


}
