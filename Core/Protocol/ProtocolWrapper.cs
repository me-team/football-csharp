﻿using ME.Football.Protocol.Message;

namespace ME.Football.Protocol
{
    /// <summary>
    /// This class contains metadata about the message, protocol.
    /// Every message should be wrapped by this class.
    /// </summary>
    public class ProtocolWrapper
    {
        public Messages MessageType { get; set; }
        public object Message { get; set; }
    }
}
