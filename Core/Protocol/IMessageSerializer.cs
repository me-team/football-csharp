﻿namespace ME.Football.Protocol
{
    /// <summary>
    /// Converts string to object. (I forgot the terminology)
    /// </summary>
    public interface IMessageSerializer
    {
        byte[] Serialize(object message);

        ProtocolWrapper DeserializerWrapper(string message);
        
        object Deserializer(string message);
    }
}