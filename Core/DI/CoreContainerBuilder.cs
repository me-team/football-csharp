﻿using Autofac;
using Serilog;

namespace ME.Football.DI
{
    public class CoreContainerBuilder : ContainerBuilder
    {
        public CoreContainerBuilder() : base()
        {
            //Magic way Microsoft Loggin
            //var services = new ServiceCollection();
            //services.AddLogging();
            //this.Populate(services);

            //Manual way Microsoft Loggin
            //this.RegisterType<LoggerFactory>()
            //    .As<ILoggerFactory>()
            //    .SingleInstance();
            //this.RegisterInstance(loggerFactory)
            //    .As<ILoggerFactory>();
            //this.RegisterGeneric(typeof(Logger<>))
            //    .As(typeof(ILogger<>))
            //    .SingleInstance();

            //Serilog


            // Add logger types
            //var loggerFactory = container.Resolve<ILoggerFactory>();
            //loggerFactory.AddConsole()
            //    .AddSerilog();
        }

        public CoreContainerBuilder UseSerilog()
        {
            this.Register<ILogger>((c, p) =>
            {
                var loggerConf = new LoggerConfiguration();
                loggerConf.WriteTo.ColoredConsole();
                return loggerConf.CreateLogger();
            });
            return this;
        }
    }
}
