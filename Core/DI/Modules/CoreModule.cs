﻿using Autofac;

namespace ME.Football.DI.Modules
{
    /// <summary>
    /// https://autofac.readthedocs.io/en/latest/configuration/modules.html
    /// </summary>
    class CoreModule : Module
    {
    }
}
