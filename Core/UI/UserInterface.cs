﻿using ME.Football.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ME.Football.UI
{
    /// <summary>
    /// You have to implement this interface in order to send updates to the UI
    /// </summary>
    public interface UserInterface
    {
        /// <summary>
        /// Triggered when team scored a goal.
        /// </summary>
        /// <param name="team">The team which scored the goal</param>
        void OnGoal(Team team);

        /// <summary>
        /// Triggered on every tick from the server.
        /// </summary>
        /// <param name="updateState">The new state of the game</param>
        void OnGameStateUpdated(/*UpdateState updateState*/);
    }
}
