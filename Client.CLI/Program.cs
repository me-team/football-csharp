﻿using System;

namespace ME.Football.Client.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new Client();

            client.Start();
            client.Loop();
        }
    }
}
