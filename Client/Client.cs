﻿using ME.Football.Protocol;
using ME.Football.Protocol.Message.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ME.Football.Protocol.Message;
using Serilog;

namespace ME.Football.Client
{
    /// <summary>
    /// Entry point of the client lib
    /// </summary>
    public class Client
    {
        private TcpClient tcpClient;
        private readonly JsonMessageSerializer serializer;

        public Client()
        {
            var log = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();
            serializer = new JsonMessageSerializer(log);
        }

        public void Start()
        {
            
        }

        public void Loop()
        {
            while (true)
            {
                tcpClient = new TcpClient();
                tcpClient.Connect(new IPEndPoint(IPAddress.Parse("192.168.0.2"), 9999));    
                Console.WriteLine(tcpClient.Connected);
                using (StreamWriter streamWriter = new StreamWriter(tcpClient.GetStream(), Encoding.ASCII))
                using (StreamReader streamReader = new StreamReader(tcpClient.GetStream(), Encoding.ASCII))
                {
                    SendBonjour(streamWriter);
                    
                    WaitAndPrintResponse(streamReader);

                    SendSelectedFormation(streamWriter);
                    
                    WaitAndPrintResponse(streamReader);
                    WaitAndPrintResponse(streamReader);
                }
                
            }
        }

        private void SendSelectedFormation(StreamWriter streamWriter)
        {
            Console.Write("Selected formation id: ");
            var name = Console.ReadLine();
            ProtocolWrapper wrapper = new ProtocolWrapper()
            {
                Message = new SelectedFormation()
                {
                    FormationId = int.Parse(name)
                },
                MessageType = Messages.SELECTED_FORMATION
            };
            SendMessage(streamWriter, wrapper);
        }

        private void WaitAndPrintResponse(StreamReader streamReader)
        {
            while (true)
            {
                if (tcpClient.GetStream().DataAvailable)
                {
                    try
                    {
                        var line = streamReader.ReadLine();
                        Console.WriteLine(line);
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }

                Task.Delay(30);
            }
        }

        private void SendBonjour(StreamWriter streamWriter)
        {
            Console.Write("Type in your name: ");
            var name = Console.ReadLine();
            ProtocolWrapper wrapper = new ProtocolWrapper()
            {
                Message = new Bonjour()
                {
                    UserName = name,
                    VersionName = "1.0.0"
                },
                MessageType = Messages.BONJORE
            };
            SendMessage(streamWriter, wrapper);
        }

        private void SendMessage(StreamWriter streamWriter, ProtocolWrapper wrapper)
        {
            var bytes = serializer.Serialize(wrapper);
            var s = Encoding.ASCII.GetString(bytes);
            streamWriter.WriteLine(s);
            streamWriter.Flush();
        }
    }
}
