﻿using ME.Football.Model;
using ME.Football.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ME.Football.Client.UI
{
    /// <summary>
    /// Extended common game events with client specific events for the UI
    /// </summary>
    interface ClientUserInterface : UserInterface
    {
        /// <summary>
        /// Triggered when server asks for select formation.
        /// </summary>
        /// <param name="formations">List of available formations (sent by server) </param>
        /// <returns>The selected formation</returns>
        //TODO: It should be some kind of async stuff
        Formation OnSelectFormation(IList<Formation> formations);
    }
}
